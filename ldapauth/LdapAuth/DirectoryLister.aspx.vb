﻿Imports System.DirectoryServices
Imports System.Data

Public Class DirectoryLister

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim users As DataTable = GetAllADUsers("gnsbs01", "OU=Users,OU=Sitekit Ltd,DC=gnhq,DC=gael,DC=net")
        GridView1.DataSource = users
        GridView1.DataBind()
    End Sub


    Private SKNameToADNameMappings(,) As String = { _
        {"Fullname", "cn"}, _
        {"Telephone", "telephonenumber"}, _
        {"Fax", "facsimileTelephoneNumber"}, _
        {"Email", "mail"}, _
        {"Department", "department"}, _
        {"Job", "title"}, _
        {"Extension Number", "pager"}, _
        {"Notes", "info"} _
    }


    Private Function GetAllADUsers(ldapServerName As String, Searchstring As String) As DataTable

        Try

            Dim lstADUsers As DataTable = New DataTable("users")
            Dim oRoot As DirectoryEntry = New DirectoryEntry("LDAP://" & ldapServerName & _
                  "/" & Searchstring)
            Dim search As DirectorySearcher = New DirectorySearcher(oRoot)
            search.Sort.PropertyName = "cn"
            search.Filter = "(&(objectClass=user)(objectCategory=person))"
            Dim RetTable As New DataTable("users")
            Dim dr As DataRow
            For i As Integer = 0 To SKNameToADNameMappings.Length / 2 - 1
                search.PropertiesToLoad.Add(SKNameToADNameMappings(i, 1))
                RetTable.Columns.Add(SKNameToADNameMappings(i, 0))
            Next

            Dim result As SearchResult
            Dim resultCol As SearchResultCollection = search.FindAll()

            If Not IsDBNull(resultCol) Then

                For Each result In resultCol
                    If result.Properties(SKNameToADNameMappings(0, 1)).Count > 0 Then
                        dr = RetTable.NewRow
                        For i As Integer = 0 To SKNameToADNameMappings.Length / 2 - 1
                            If result.Properties(SKNameToADNameMappings(i, 1)).Count > 0 Then
                                dr(SKNameToADNameMappings(i, 0)) = result.Properties(SKNameToADNameMappings(i, 1))(0)
                            End If
                        Next
                        RetTable.Rows.Add(dr)
                    End If
                Next
            End If
            Return RetTable

        Catch ex As Exception
            Throw ex
        End Try
    End Function


End Class