﻿Contain CS and VB version of the same function with following difference
CS version of directory list support JSN calls VB version does not

23/3/2016
1. the AD to field mappings have been externalised to the web.config to allow them to be edited in situ and are applied to auth and listing services
2. a VB version of directory list has been added but without the JSoN support seen in the CS version
3. web.configs have been made the same for cs and vb
4. Directory list now supports additional key searchFilter applied to all methods, default is (&amp;(objectClass=contact)(objectCategory=person)) however
its worth looking at the AD via softerra as often it can be (&amp;(objectClass=user)(objectCategory=person))  etc
5. note if retrning contact then remove sAMAccountName as the forst field in web.config as contact have no equivalent 