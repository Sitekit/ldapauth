﻿Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.ComponentModel
Imports System.Web
Imports System
Imports System.Collections
Imports System.Xml
Imports System.Configuration
Imports System.DirectoryServices



<System.Web.Services.WebService(Namespace:="http://www.sitekit.net/LDAPUtility/")> _
<System.Web.Services.WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<ToolboxItem(False)> _
Public Class LDAPAuthV6
    Inherits System.Web.Services.WebService

    Dim groups As New ArrayList
    Dim entry, rootentry As DirectoryEntry

    <WebMethod(Description:="simple test taking in username password and ldap server eg  LDAP://gnsbs01.  nothing from web.config")> _
    Public Function testAuthSimple(username As String, password As String, ldapserver As String) As [String]
        ' create representation of entry in Active Directory
        entry = New DirectoryEntry(ldapserver, username, password)
        'entry=new DirectoryEntry(ldapserver, username, password,AuthenticationTypes.Anonymous);

        ' Bind entry to the native AdsObject to force authentication.
        Dim obj As Object = entry.NativeObject
        Return entry.ToString()
    End Function






    <WebMethod(Description:="Authenticates specified user details against specified LDAP server. This function with authenticated users in nested groups, too. Can not check against groups like Domain Users for this case leave group empty and it only authenticates.")> _
    Public Function IsAuthenticated(ByVal username As String, ByVal password As String, ByVal group As String) As XmlDocument
        Dim LDAPServers As String()
        Dim DomainPrefix As String()
        Dim Result As New XmlDocument
        LDAPServers = System.Configuration.ConfigurationManager.AppSettings("LDAPServers").ToString.Split(",")
        DomainPrefix = System.Configuration.ConfigurationManager.AppSettings("DomainPrefix").ToString.Split(",")
        For i As Int16 = 0 To LDAPServers.Length - 1
            Dim UsernameToCheck As String
            If Not UsernameHasDomain(username) Then
                UsernameToCheck = DomainPrefix(i).Trim & "\" & username.Trim
            Else
                UsernameToCheck = username
            End If
            Result = IsAuthenticatedAgainstLDAP(UsernameToCheck, password, group, LDAPServers(i))
            If Result.InnerText = True.ToString Then Exit For
        Next
        Return Result
    End Function

    <WebMethod(Description:="Returns all groups of an specified user. This function with authenticated users in nested groups, too. If ReturnError is set to true a error is returned from each server the user has no rights on, but it's good for debugging.")> _
    Public Function ReturnGroups(ByVal username As String, ByVal password As String) As String
        Dim LDAPServers As String()
        Dim DomainPrefix As String()
        Dim ReturnError As Boolean
        Dim Results As String = ""
        ReturnError = CBool(System.Configuration.ConfigurationManager.AppSettings("ReturnError").ToString)
        LDAPServers = System.Configuration.ConfigurationManager.AppSettings("LDAPServers").ToString.Split(",")
        DomainPrefix = System.Configuration.ConfigurationManager.AppSettings("DomainPrefix").ToString.Split(",")
        For i As Int16 = 0 To LDAPServers.Length - 1
            Dim UsernameToCheck As String
            If Not UsernameHasDomain(username) Then
                UsernameToCheck = DomainPrefix(i).Trim & "\" & username.Trim
            Else
                UsernameToCheck = username
            End If
            Results &= ReturnGroupsAgainstLDAP(UsernameToCheck, password, LDAPServers(i), ReturnError)
        Next
        Return Results
    End Function

    ''' <summary>
    ''' Authenticates against a given LDAP server. Get's all the groups associated (including nested supergroups. Can not check against groups like "Domain Users" for this case leave group empty and it only authenticates.
    ''' </summary>
    ''' <param name="username"></param>
    ''' <param name="password"></param>
    ''' <param name="group"></param>
    ''' <param name="LDAPServer"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function IsAuthenticatedAgainstLDAP(ByVal username As String, ByVal password As String, ByVal group As String, ByVal LDAPServer As String) As XmlDocument

        ' get path to LDAP server from web.config
        Dim path As String = LDAPServer
        Dim usernameWithoutDomain As String

        Try
            usernameWithoutDomain = getUsernameWithoutDomain(username)

            ' create representation of entry in Active Directory
            entry = New DirectoryEntry(path, username, password)

            ' Bind entry to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject

            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & usernameWithoutDomain & ")"  'this can be extended to accomodate more filters
            search.PropertiesToLoad.Add("cn")
            'search.PropertiesToLoad.Add("telephoneNumber")
            Dim result As SearchResult = search.FindOne()



            If (result Is Nothing) Then
                Return ResultXML(False, "No user found or no connection to the LDAP server")
            Else
                If group <> "" Then
                    ' Dim groups As ArrayList
                    GetGroups(result, username, password, LDAPServer)
                    If groups.Contains(group.ToLower) Then
                        Return ResultXML(True, "Success, user found in " & group.ToLower)
                    Else

                        Return ResultXML(False, "User found, but this user is not part of the queried group")
                    End If
                Else
                    Return ResultXML(True, "Success, user found in Domain users")
                End If

            End If

        Catch ex As Exception
            Return ResultXML(False, "Error: " & ex.ToString)
            'Throw ex
        End Try

    End Function

#Region "Integrated Active Directory"

    <WebMethod(Description:="Authenticates specified user details against specified LDAP server. This function with authenticated users in nested groups, too. Can not check against groups like Domain Users for this case leave group empty and it only authenticates.")> _
    Public Function GetActiveDirectoryEntry(ByVal username As String, ByVal password As String, ByVal group As String) As XmlDocument
        Dim LDAPServers As String()
        Dim DomainPrefix As String()
        Dim Result As New XmlDocument
        LDAPServers = System.Configuration.ConfigurationManager.AppSettings("LDAPServers").ToString.Split(",")
        DomainPrefix = System.Configuration.ConfigurationManager.AppSettings("DomainPrefix").ToString.Split(",")
        For i As Int16 = 0 To LDAPServers.Length - 1
            Dim UsernameToCheck As String
            If Not UsernameHasDomain(username) Then
                UsernameToCheck = DomainPrefix(i).Trim & "\" & username.Trim
            Else
                UsernameToCheck = username
            End If
            Result = GetActiveDirectoryXML(UsernameToCheck, password, group, LDAPServers(i))
            If Result.InnerText = True.ToString Then Exit For
        Next
        Return Result
    End Function

    Private Function GetActiveDirectoryXML(ByVal username As String, ByVal password As String, ByVal group As String, ByVal LDAPServer As String) As XmlDocument

        ' get path to LDAP server from web.config
        Dim path As String = LDAPServer
        Dim usernameWithoutDomain As String

        Try
            usernameWithoutDomain = getUsernameWithoutDomain(username)

            ' create representation of entry in Active Directory
            entry = New DirectoryEntry(path, username, password)

            ' Bind entry to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject

            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & usernameWithoutDomain & ")"

            'revised to use the name value pair collection
            Dim NameToADNameCol As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
            Dim s As String
            For Each s In NameToADNameCol.AllKeys
                search.PropertiesToLoad.Add(NameToADNameCol(s))
            Next s
            Dim result As SearchResult = search.FindOne()

            If (result Is Nothing) Then
                Return ResultXML(False, "No user found or no connection to the LDAP server")
            Else
                If group <> "" Then
                    ' Dim groups As ArrayList
                    GetGroups(result, username, password, LDAPServer)
                    If groups.Contains(group.ToLower) Then
                        Return ADUserXML(True, result)
                    Else

                        Return ResultXML(False, "User found, but this user is not part of the queried group")
                    End If
                Else
                    Return ADUserXML(True, result)
                End If

            End If

        Catch ex As Exception
            Return ResultXML(False, "Error: " & ex.ToString)
            'Throw ex
        End Try

    End Function

    ' returns 'true' or 'false' in an xml document (format expected by Sitekit 3rd Party Authentication)
    Private Function ADUserXML(ByVal authentic As Boolean, ByRef result As SearchResult) As XmlDocument

        '<user>" & _
        '                                    "   <authenticated>true</authenticated>" & _
        '                                    "   <userparam name=""Fullname"">Andy Stables</userparam>" & _
        '                                    "   <userparam name=""Eye Colour"">Dingy</userparam>" & _
        '                                    "</user>
        Dim xmlDoc As New XmlDocument()
        Dim authenticatedElement As XmlElement
        Dim rootElement As XmlElement

        rootElement = xmlDoc.CreateElement("user")

        authenticatedElement = xmlDoc.CreateElement("authenticated")
        authenticatedElement.InnerText = authentic.ToString()
        rootElement.AppendChild(authenticatedElement)

        'revised to use the name value pair collection
        Dim NameToADNameCol As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
        Dim s As String
        For Each s In NameToADNameCol.AllKeys
            Dim userParamElement As XmlElement = xmlDoc.CreateElement("userparam")
            userParamElement.SetAttribute("name", s)
            If result.Properties(NameToADNameCol(s)).Count > 0 Then
                userParamElement.InnerText = result.Properties(NameToADNameCol(s))(0)
                rootElement.AppendChild(userParamElement)
            End If
        Next s


        'this section added from v5 version to v4
        Dim groupsNode As XmlElement = xmlDoc.CreateElement("groups")

        For Each groupName As String In groups
            Dim group As XmlElement = xmlDoc.CreateElement("group")
            group.InnerText = groupName
            groupsNode.AppendChild(group)
        Next
        rootElement.AppendChild(groupsNode)
        'end section

        xmlDoc.AppendChild(rootElement)

        Return xmlDoc

    End Function

#End Region

    ' retrieves all groups specified user is a member of
    Private Sub GetGroups(ByVal userResult As SearchResult, ByVal username As String, ByVal password As String, ByVal LDAPServer As String)

        Dim cn As String ' "common name" - LDAP attribute made from givenName joined to SN (surname)
        Dim dn As String ' "distinguished name" - LDAP attribute that uniquely defines an object in Active Directory
        Dim groupName As String ' name of group in Active Directory
        ' Dim groups As New ArrayList

        Dim search As DirectorySearcher
        Dim result As SearchResult

        Dim equalsIndex, commaIndex, propertyCounter As Integer

        Try
            ' create representation of entry in Active Directory
            entry = New DirectoryEntry(userResult.Path, username, password)

            ' Bind entry to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject

            ' do full search on specified user to retrieve those groups user is a member of
            search = New DirectorySearcher(entry)
            cn = CType(userResult.Properties.Item("cn").Item(0), String)
            search.Filter = "(cn=" & cn & ")"
            search.PropertiesToLoad.Add("memberOf")
            result = search.FindOne()

            ' extract the group name from each dn 
            For propertyCounter = 0 To result.Properties("memberOf").Count - 1
                dn = CType(result.Properties.Item("memberOf").Item(propertyCounter), String)

                ' extract 1st attribute from dn
                equalsIndex = dn.IndexOf("=", 1)
                commaIndex = dn.IndexOf(",", 1)
                If (equalsIndex <> -1) Then
                    groupName = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1)
                    'only add if it's not in the array list already
                    If Not groups.Contains(groupName.ToLower) Then
                        groups.Add(groupName.ToLower)
                        'seach for this group now
                        'get a root entry to search ALL LDAP for this group not jsut under the user
                        Dim path As String = LDAPServer

                        ' create representation of entry in Active Directory
                        rootentry = New DirectoryEntry(path, username, password)

                        ' Bind entry to the native AdsObject to force authentication.
                        obj = rootentry.NativeObject

                        Dim search1 As DirectorySearcher = New DirectorySearcher(rootentry)
                        search1.Filter = "(SAMAccountName=" & groupName.ToLower & ")"
                        search1.PropertiesToLoad.Add("cn")
                        Dim result1 As SearchResult = search1.FindOne()
                        If Not (result1 Is Nothing) Then GetGroups(result1, username, password, LDAPServer)
                    Else 'if it's in the arraylist already finish loop
                        ' Exit For
                    End If
                End If

            Next propertyCounter

        Catch ex As Exception
            Throw New Exception("Error obtaining group names. " & ex.Message)
        End Try



    End Sub


    ' retrieves all groups specified user is a member of
    Private Function GetGroupsV2(ByVal userResult As SearchResult) As ArrayList

        Dim cn As String ' "common name" - LDAP attribute made from givenName joined to SN (surname)
        Dim dn As String ' "distinguished name" - LDAP attribute that uniquely defines an object in Active Directory
        Dim groupName As String ' name of group in Active Directory
        Dim groups As New ArrayList
        Dim search As DirectorySearcher
        Dim resultCollection As SearchResultCollection
        Dim equalsIndex As Integer
        Dim commaIndex As Integer
        Dim propertyCounter As Integer

        ' Do full search on specified user to retrieve those groups user is a member of
        search = New DirectorySearcher(userResult.Path)
        cn = CType(userResult.Properties.Item("cn").Item(0), String)
        search.Filter = "(cn=" & cn & ")"
        search.PropertiesToLoad.Add("memberOf")

        resultCollection = search.FindAll()

        For Each result As SearchResult In resultCollection
            ' Extract the group name from each dn 
            For propertyCounter = 0 To result.Properties("memberOf").Count - 1

                dn = CType(result.Properties.Item("memberOf").Item(propertyCounter), String)

                ' Extract 1st attribute from dn
                equalsIndex = dn.IndexOf("=", 1)
                commaIndex = dn.IndexOf(",", 1)

                If (equalsIndex <> -1) Then
                    groupName = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1)
                    groups.Add(groupName.ToLower)
                End If
            Next propertyCounter
        Next

        Return groups
    End Function



    ' returns 'true' or 'false' in an xml document (format expected by Sitekit 3rd Party Authentication)
    Private Function ResultXML(ByVal authenticated As Boolean, Optional ByVal reason As String = "") As XmlDocument

        Dim xmlDoc As New XmlDocument
        Dim elAuth As XmlElement

        elAuth = xmlDoc.CreateElement("authenticated")
        elAuth.InnerText = authenticated.ToString()
        If reason <> "" Then
            elAuth.SetAttribute("Reason", reason.ToString())
        End If
        xmlDoc.AppendChild(elAuth)


        Return xmlDoc

    End Function





    Private Function ReturnGroupsAgainstLDAP(ByVal username As String, ByVal password As String, ByVal LDAPServer As String, ByVal ReturnError As Boolean) As String

        ' get path to LDAP server from web.config
        Dim path As String = LDAPServer
        Dim usernameWithoutDomain As String

        Try
            usernameWithoutDomain = getUsernameWithoutDomain(username)

            ' create representation of entry in Active Directory
            entry = New DirectoryEntry(path, username, password)

            ' Bind entry to the native AdsObject to force authentication.
            Dim obj As Object = entry.NativeObject

            Dim search As DirectorySearcher = New DirectorySearcher(entry)
            search.Filter = "(SAMAccountName=" & usernameWithoutDomain & ")"
            search.PropertiesToLoad.Add("cn")
            Dim result As SearchResult = search.FindOne()


            If (result Is Nothing) Then
                Return "No user found or no connection to the LDAP server"
            Else
                ' Dim groups As ArrayList
                GetGroups(result, username, password, LDAPServer)
                Dim res As String = ""
                For Each s As String In groups
                    res = res & ":" & s
                Next

                res &= "EMAIL = " & entry.Properties("mail").Value
                res &= "EMAIL = " & entry.Properties("cnn").Value

                Return res
            End If

        Catch ex As Exception
            If ReturnError Then
                Return "Error: " & ex.ToString
            Else
                Return ""
            End If
        End Try

    End Function


    Private Function getUsernameWithoutDomain(ByVal username As String) As String
        If username.IndexOf("\") > -1 Then
            Return username.Substring(username.IndexOf("\") + 1)
        Else
            Return username
        End If
    End Function

    Private Function UsernameHasDomain(ByVal username As String) As Boolean
        If username.IndexOf("\") > -1 Then
            Return True
        Else
            Return False
        End If
    End Function

End Class