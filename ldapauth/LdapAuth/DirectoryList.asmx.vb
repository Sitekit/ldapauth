﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Web.Services
Imports System.Text
Imports System.Collections
Imports System.Data
Imports System.Diagnostics
Imports System.DirectoryServices
Imports System.Xml
Imports System.Collections.Specialized


''This code is intended to run on the owners AD system and expose their AD to searching and listing 

<WebService([Namespace]:="http://www.sitekit.net/LDAPUtility/", Description:="Provides methods for searching and listing users from an active directory")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.ComponentModel.ToolboxItem(False)> _
<System.Web.Script.Services.ScriptService> _
Public Class DirectoryList
    Inherits System.Web.Services.WebService



#Region "datatable web methods"
    <WebMethod(Description:="Returns all AD users as datatable")> _
    Public Function ListFullDirectory() As DataTable
        Return GetADAllUsers()
    End Function

    <WebMethod(Description:="Returns all AD users with Fullname(cn) begining with startletter as datatable")> _
    Public Function AtoZDirectory(StartLetter As [Char]) As DataTable
        Return GetADAtoZ(StartLetter)
    End Function

    <WebMethod(Description:="Returns all AD users with a wildcard search of searchText in a single field searchfield as datatable")> _
    Public Function SearchDirectory(searchText As String, searchField As String) As DataTable
        Return GetADsearch(searchText, searchField)
    End Function

    <WebMethod(Description:="Returns all AD users with a wildcard search of searchText from firstname, surname and a contcatenated bothnames as datatable")> _
    Public Function SearchDirectoryAsDT(searchText As String) As DataTable
        Return GetADDataTableSearch(searchText)
    End Function

    <WebMethod(Description:="Returns a single AD user via their unique identifier (sAMAccountName) field")> _
    Public Function getUser(ID As String) As DataTable
        Return GetSingleUser(ID)
    End Function
#End Region

#Region "XML web methods"

    <WebMethod(Description:="Returns all AD users as an XML doc")> _
    Public Function ListFullDirectoryXML() As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DTtoXML(GetADAllUsers())
        Return XMLDoc
    End Function

    <WebMethod(Description:="Returns all AD users with Fullname(cn) begining with startletter as XML doc")> _
    Public Function AtoZDirectoryXML(StartLetter As [Char]) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DTtoXML(GetADAtoZ(StartLetter))
        Return XMLDoc
    End Function

    <WebMethod(Description:="Returns all AD users with a wildcard search of searchText in a single field searchfield as XML doc")> _
    Public Function SearchDirectoryXML(searchText As String, searchField As String) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DTtoXML(GetADsearch(searchText, searchField))
        Return XMLDoc
    End Function

    <WebMethod(Description:="Returns all AD users with a wildcard search of searchText from firstname, surname and a concatenated bothnames as XML doc")> _
    Public Function SearchDirectoryasDTXML(searchText As String) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DTtoXML(GetADDataTableSearch(searchText))
        Return XMLDoc
    End Function

    <WebMethod(Description:="Returns a single AD user via their unique identifier (sAMAccountName) field as XML doc")> _
    Public Function getUserXML(ID As String) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DTtoXML(GetSingleUser(ID))
        Return XMLDoc
    End Function
#End Region




    Private Function GetADAllUsers() As DataTable

        Try
            'move thes insidee try catch
            Dim ldapServerName As String = System.Configuration.ConfigurationManager.AppSettings("LDAPServer").ToString()
            Dim Searchstring As String = System.Configuration.ConfigurationManager.AppSettings("searchString").ToString()
            Dim SearchFilter As String = System.Configuration.ConfigurationManager.AppSettings("searchFilter").ToString()

            Dim oRoot As New DirectoryEntry(Convert.ToString((Convert.ToString("LDAP://") & ldapServerName) + "/") & Searchstring)
            Dim search As New DirectorySearcher(oRoot)
            search.Sort.PropertyName = "cn"
            search.Filter = Replace(SearchFilter, "&amp;", "&")
            Dim RetTable As New DataTable("users")
            Dim dr As DataRow = Nothing

            Dim adsection As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
            For Each key As String In adsection
                search.PropertiesToLoad.Add(adsection(key))
                RetTable.Columns.Add(key)
            Next

            Dim resultCol As SearchResultCollection = search.FindAll()


            If resultCol IsNot Nothing Then
                For Each result As SearchResult In resultCol
                    If result.Properties(adsection(0)).Count > 0 Then
                        dr = RetTable.NewRow()
                        For Each key As String In adsection
                            If result.Properties(adsection(key)).Count > 0 Then
                                dr(key) = result.Properties(adsection(key))(0)
                            End If
                        Next
                        RetTable.Rows.Add(dr)
                    End If
                Next
            End If

            RetTable.AcceptChanges()

            Return RetTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Private Function GetADAtoZ(Optional StartCharacter As Char = " "c) As DataTable
        Dim ldapServerName As String = System.Configuration.ConfigurationManager.AppSettings("LDAPServer").ToString()
        Dim Searchstring As String = System.Configuration.ConfigurationManager.AppSettings("searchString").ToString()
        Dim SearchFilter As String = System.Configuration.ConfigurationManager.AppSettings("searchFilter").ToString()

        Try
            If (Not [String].IsNullOrEmpty(ldapServerName) AndAlso (Not [String].IsNullOrEmpty(Searchstring))) Then
                Dim oRoot As New DirectoryEntry(Convert.ToString((Convert.ToString("LDAP://") & ldapServerName) + "/") & Searchstring)
                Dim search As New DirectorySearcher(oRoot)
                search.Sort.PropertyName = "cn"
                search.Filter = Replace(SearchFilter, "&amp;", "&")
                If StartCharacter <> " "c Then
                    search.Filter = "(&(" + search.Filter + ")(cn=" + StartCharacter + "*))"
                End If
                Dim RetTable As New DataTable("users")
                Dim dr As DataRow = Nothing
                Dim adsection As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
                For Each key As String In adsection
                    search.PropertiesToLoad.Add(adsection(key))
                    RetTable.Columns.Add(key)
                Next

                Dim resultCol As SearchResultCollection = search.FindAll()


                If resultCol IsNot Nothing Then
                    For Each result As SearchResult In resultCol
                        If result.Properties(adsection(0)).Count > 0 Then
                            dr = RetTable.NewRow()
                            For Each key As String In adsection
                                If result.Properties(adsection(key)).Count > 0 Then
                                    dr(key) = result.Properties(adsection(key))(0)
                                End If
                            Next
                            RetTable.Rows.Add(dr)
                        End If
                    Next
                End If

                Return RetTable
            Else
                Throw New KeyNotFoundException("Null or empty web.config values for 'LDAPServer' and/or 'searchString'")

            End If
        Catch ex As Exception
            Throw ex
        End Try



    End Function

    Private Function GetADsearch(searchFor As String, searchField As String) As DataTable
        Dim ldapServerName As String = System.Configuration.ConfigurationManager.AppSettings("LDAPServer").ToString()
        Dim Searchstring As String = System.Configuration.ConfigurationManager.AppSettings("searchString").ToString()
        Dim SearchFilter As String = System.Configuration.ConfigurationManager.AppSettings("searchFilter").ToString()
        Try
            Dim oRoot As New DirectoryEntry(Convert.ToString((Convert.ToString("LDAP://") & ldapServerName) + "/") & Searchstring)
            Dim search As New DirectorySearcher(oRoot)
            search.Sort.PropertyName = "cn"
            search.Filter = Replace(SearchFilter, "&amp;", "&")
            If (String.Compare(searchFor, "") > 0) AndAlso (String.Compare(searchField, "") > 0) Then
                search.Filter = (Convert.ToString((Convert.ToString("(&(" + search.Filter + ")(") & searchField) + "=*") & searchFor) + "*))"
            End If
            Dim RetTable As New DataTable("users")
            Dim dr As DataRow = Nothing
            Dim adsection As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
            For Each key As String In adsection
                search.PropertiesToLoad.Add(adsection(key))
                RetTable.Columns.Add(key)
            Next

            Dim resultCol As SearchResultCollection = search.FindAll()


            If resultCol IsNot Nothing Then
                For Each result As SearchResult In resultCol
                    If result.Properties(adsection(0)).Count > 0 Then
                        dr = RetTable.NewRow()
                        For Each key As String In adsection
                            If result.Properties(adsection(key)).Count > 0 Then
                                dr(key) = result.Properties(adsection(key))(0)
                            End If
                        Next
                        RetTable.Rows.Add(dr)
                    End If
                Next
            End If

            Return RetTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function GetADDataTableSearch(searchFor As String) As DataTable
        'this Tries to get around the full search option wherby an LDAP search of both name eg 'Niall Till' would retrn nothing
        'it uses the previously created DT talber, lodas it locally and then filter it on firstname, surname and the concatenated bothname field

        Dim newTable As DataTable
        Dim AllUsers As DataTable = GetADAllUsers()
        AllUsers.Columns.Add("Bothnames", GetType(String))
        AllUsers.AcceptChanges()
        newTable = AllUsers.Clone()
        'to get the same table structure but should be empty
        For Each row As DataRow In AllUsers.Rows
            row("Bothnames") = (row("FirstName").ToString() + " " + row("Surname").ToString()).Trim()
        Next
        AllUsers.AcceptChanges()

        Dim dv As New DataView(AllUsers)
        dv.RowFilter = (Convert.ToString((Convert.ToString((Convert.ToString("FirstName LIKE '%") & searchFor) + "%' OR Surname LIKE '%") & searchFor) + "%' OR Bothnames LIKE '%") & searchFor) + "%' "
        dv.Sort = "Surname ASC"
        For Each dr As DataRow In dv.ToTable().Rows
            newTable.ImportRow(dr)
        Next
        newTable.AcceptChanges()
        Return newTable
    End Function


    Private Function GetSingleUser(id As String) As DataTable
        Dim ldapServerName As String = System.Configuration.ConfigurationManager.AppSettings("LDAPServer").ToString()
        Dim Searchstring As String = System.Configuration.ConfigurationManager.AppSettings("searchString").ToString()
        Dim SearchFilter As String = System.Configuration.ConfigurationManager.AppSettings("searchFilter").ToString()

        Try
            Dim oRoot As New DirectoryEntry(Convert.ToString((Convert.ToString("LDAP://") & ldapServerName) + "/") & Searchstring)
            Dim search As New DirectorySearcher(oRoot)
            search.Filter = Replace(SearchFilter, "&amp;", "&")
            If (String.Compare(id, "") > 0) Then
                search.Filter = (Convert.ToString("(&(" + search.Filter + ")(sAMAccountName=") & id) + "))"
            End If
            Dim RetTable As New DataTable("users")
            Dim dr As DataRow = Nothing
            Dim adsection As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
            For Each key As String In adsection
                search.PropertiesToLoad.Add(adsection(key))
                RetTable.Columns.Add(key)
            Next

            Dim resultCol As SearchResultCollection = search.FindAll()


            If resultCol IsNot Nothing Then
                For Each result As SearchResult In resultCol
                    If result.Properties(adsection(0)).Count > 0 Then
                        dr = RetTable.NewRow()
                        For Each key As String In adsection
                            If result.Properties(adsection(key)).Count > 0 Then
                                dr(key) = result.Properties(adsection(key))(0)
                            End If
                        Next
                        RetTable.Rows.Add(dr)
                    End If
                Next
            End If


            Return RetTable
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Function GetADAllUsers() As DataTable
    '    Dim ArrLdapServers() As String
    '    Dim ArrSearchStrings() As String
    '    Try
    '        'move these inside try catch
    '        ArrLdapServers = System.Configuration.ConfigurationManager.AppSettings("LDAPServer").ToString.Split("#")
    '        ArrSearchStrings = System.Configuration.ConfigurationManager.AppSettings("searchString").ToString.Split("#")

    '        'loop through array of servers and search strings
    '        Dim dtAllUsers As New DataTable()
    '        For i As Int16 = 0 To ArrLdapServers.Length - 1
    '            Dim dtSingleSearchUsers As DataTable = GetTableforSingleSearch(ArrLdapServers(i), ArrSearchStrings(i))
    '            dtAllUsers.Merge(dtSingleSearchUsers)
    '        Next
    '        dtAllUsers.AcceptChanges()
    '        Dim view As DataView = dtAllUsers.DefaultView
    '        view.Sort = "Fullname"  'need to check
    '        Return view.ToTable()

    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Function


    'Private Function GetTableforSingleSearch(ldapServerName As String, Searchstring As String) As DataTable

    '    Dim ssConfigValue As String = System.Configuration.ConfigurationManager.AppSettings("SearchScope").ToString()

    '    Dim oRoot As New DirectoryEntry(Convert.ToString((Convert.ToString("LDAP://") & ldapServerName) + "/") & Searchstring)
    '    Dim search As New DirectorySearcher(oRoot)
    '    search.Sort.PropertyName = "cn"
    '    search.Filter = "(&(objectClass=user)(objectCategory=person))"
    '    If ssConfigValue = "OneLevel" Or ssConfigValue = "Subtree" Then  'only apply if not base
    '        Dim value As SearchScope = DirectCast([Enum].Parse(GetType(SearchScope), ssConfigValue), SearchScope)
    '        search.SearchScope = value
    '    End If
    '    Dim RetTable As New DataTable("users")
    '    Dim dr As DataRow = Nothing

    '    Dim adsection As NameValueCollection = DirectCast(System.Configuration.ConfigurationManager.GetSection("ADMap"), NameValueCollection)
    '    For Each key As String In adsection
    '        search.PropertiesToLoad.Add(adsection(key))
    '        RetTable.Columns.Add(key)
    '    Next

    '    Dim resultCol As SearchResultCollection = search.FindAll()


    '    If resultCol IsNot Nothing Then
    '        For Each result As SearchResult In resultCol
    '            If result.Properties(adsection(0)).Count > 0 Then
    '                dr = RetTable.NewRow()
    '                For Each key As String In adsection
    '                    If result.Properties(adsection(key)).Count > 0 Then
    '                        dr(key) = result.Properties(adsection(key))(0)
    '                    End If
    '                Next
    '                RetTable.Rows.Add(dr)
    '            End If
    '        Next
    '    End If

    '    RetTable.AcceptChanges()

    '    Return RetTable

    'End Function


#Region "utilities"

    Public Function DTtoXML(dtIn As DataTable) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        Dim ds As New DataSet("directory")
        ds.Tables.Add(dtIn)
        ds.AcceptChanges()
        Dim strXML As String = ds.GetXml()
        XMLDoc.LoadXml(strXML)
        Return XMLDoc
    End Function


#End Region

End Class




