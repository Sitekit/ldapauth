<%@ WebService Language="vb" Class="DirectoryListNELFT" %>

Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System
Imports System.XML
Imports System.Data
Imports System.Data.SqlClient

''' <summary>
''' v1.0
''' Created 29/4/2014 specifically for NELFT to extract user from a teemporary table populated from 2 ADs
''' to be superceded late 2014 when SWECS and NEL ADs are merged  
''' </summary>
''' <remarks></remarks>
<WebService(Namespace:="http://www.sitekit.net/LDAPUtility/")> _
Public Class DirectoryListNELFT
 
    Inherits System.Web.Services.WebService
 
    Dim STAFFDB_ConnectionString As String = "data source=10.207.215.201;initial Catalog=NELFTAD;password=qDfuaEM;persist security info=True;user id=nelft;Max Pool Size=300; "
 
    <WebMethod(Description:="Returns a single AD user via their unique identifier (sAMAccountName) field as XML doc")> _
    Public Function getUserXML(ByVal ID As String) As XmlDocument        
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DStoXML(GetSingleUser(ID))
        Return XMLDoc
    End Function

    Private Function GetSingleUser(ByVal thisid As String) As DataSet
        Dim ds As New DataSet

        Dim sql As String = "SELECT * from NELFTActiveDirectory WHERE Username = '" & thisid & "' order by surname"
        Dim sqlConnection As SqlConnection
        sqlConnection = New SqlConnection(STAFFDB_ConnectionString)
        Dim sqlCommand As New SqlCommand(sql, sqlConnection)
        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        dataAdapter.Fill(ds, "directory")
        ds.AcceptChanges()
        Return ds
        
    End Function




    <WebMethod(Description:="Returns all AD users with a wildcard search of searchText in a single field searchfield as XML doc")> _
    Public Function SearchDirectoryXML(ByVal searchText As String, ByVal searchField As String) As XmlDocument        
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DStoXML(GetADsearch(searchText, searchField))
        Return XMLDoc
    End Function
    
    Private Function GetADsearch(ByVal searchTxt As String, ByVal searchFld As String) As DataSet
        Dim ds As New DataSet

        Dim sql As String = "SELECT * from NELFTActiveDirectory WHERE searchFld like '%" & searchTxt & "%' order by surname"
        If searchFld = "" Then  'search all likely fields
            sql = "SELECT * from NELFTActiveDirectory WHERE Surname like '%" & searchTxt & "%' or FirstName like '%" & searchTxt & "%'  or email like '%" & searchTxt & "%' or JobTitle like '%" & searchTxt & "%' or FirstName+' '+Surname='" & searchTxt & "' order by surname"
        End If
        Dim sqlConnection As SqlConnection
        sqlConnection = New SqlConnection(STAFFDB_ConnectionString)
        Dim sqlCommand As New SqlCommand(sql, sqlConnection)
        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        dataAdapter.Fill(ds, "directory")
        ds.AcceptChanges()
        Return ds
        
    End Function

    
    
    <WebMethod(Description:="Returns all AD users with Surname begining with startletter as XML doc")> _
    Public Function AtoZDirectoryXML(ByVal StartLetter As Char) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        XMLDoc = DStoXML(GetADAtoZ(StartLetter))
        Return XMLDoc
    End Function
    
    Private Function GetADAtoZ(Optional StartCharacter As Char = " ") As DataSet
        Dim ds As New DataSet

        Dim sql As String = "SELECT * from NELFTActiveDirectory WHERE surname like '" & StartCharacter & "%' order by surname"
        Dim sqlConnection As SqlConnection
        sqlConnection = New SqlConnection(STAFFDB_ConnectionString)
        Dim sqlCommand As New SqlCommand(sql, sqlConnection)
        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(sqlCommand)

        dataAdapter.Fill(ds, "directory")
        ds.AcceptChanges()
        Return ds
        
    End Function

    
    

    
    Public Function DStoXML(ds As DataSet) As XmlDocument
        Dim XMLDoc As New XmlDocument()
        ds.DataSetName = "directory"
        ds.Tables(0).TableName = "users"
        ds.AcceptChanges()
        Dim strXML As String = ds.GetXml()
        XMLDoc.LoadXml(strXML)
        Return XMLDoc
    End Function


End Class