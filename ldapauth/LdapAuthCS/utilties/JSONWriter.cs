﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Text;

namespace LdapAuthCS.utilties
{
    public class JSONWriter
    {

	    private StringBuilder _JSON = new StringBuilder();
        private int[] _noOfProperties = new int[11];
        private int[] _noOfArrayElements = new int[11];

        public int _level = 0;

	    public JSONWriter()
	    {
		    //Start the JSON object.
		    _JSON.Append("{");

		    for (int i = 0; i <= 10; i++) {
			    _noOfProperties[i] = 0;
			    _noOfArrayElements[0] = 0;
		    }
	    }


	    public void AddProperty(string name, string value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\":\"" + EscapeQuotes(value) + "\"");

	    }


	    public void AddBooleanProperty(string name, bool value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\":" + value.ToString().ToLower());

	    }


	    public void AddIntegerProperty(string name, int value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\":" + value.ToString());

	    }


	    public void AddObjectProperty(string name, string value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\":" + value);

	    }


	    public void BeginArray(string name)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\"" + ":[");

		    _level += 1;
		    _noOfProperties[_level] = 0;

	    }


	    public void AddArrayElement(string value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + EscapeQuotes(value) + "\"");

	    }


	    public void AddObjectArrayElement(string value)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append(EscapeQuotes(value));

	    }


	    public void EndArray()
	    {
		    _JSON.Append("]");
		    _level -= 1;

	    }


	    public void BeginObject(string name)
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("\"" + name + "\"" + ":{");

		    _level += 1;
		    _noOfProperties[_level] = 0;

	    }


	    public void BeginObject()
	    {
		    _noOfProperties[_level] += 1;

		    if (_noOfProperties[_level] > 1) {
			    _JSON.Append(",");
		    }

		    _JSON.Append("{");

		    _level += 1;
		    _noOfProperties[_level] = 0;

	    }


	    public void EndObject()
	    {
		    _level -= 1;

		    _JSON.Append("}");

	    }

	    public string GetJSON()
	    {
		    //End the JSON object.
		    _JSON.Append("}");
		    string json = _JSON.ToString();
		    return json;
	    }

	    private string EscapeQuotes(string value)
	    {

            return value.Replace("\"", "\\\"").Replace(System.Environment.NewLine, "");

	    }

    }
}