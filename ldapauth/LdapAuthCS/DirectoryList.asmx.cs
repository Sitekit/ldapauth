﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Text;
using System.Collections;
using System.Data;
using System.Diagnostics;
using System.DirectoryServices;
using System.Xml;
using LdapAuthCS.utilties;
using System.Collections.Specialized;

namespace LdapAuthCS
{
    /// <summary>
    /// This code is intended to run on the owners AD system and expose the 
    [WebService(Namespace = "http://www.sitekit.net/LDAPUtility/", Description = "Provides methods for searching and listing users from an active directory")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class DirectoryList : System.Web.Services.WebService
    {
        //private int outlineListingFieldtoShow = 5;   //show first 5 fields of the array in list view


        #region datatable web methods
        [WebMethod(Description = "Returns all AD users as datatable")]
        public DataTable ListFullDirectory()
        {
            return GetADAllUsers();
        }

        [WebMethod(Description = "Returns all AD users with Fullname(cn) begining with startletter as datatable")]
        public DataTable AtoZDirectory(Char StartLetter)
        {
            return GetADAtoZ(StartLetter);
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText in a single field searchfield as datatable")]
        public DataTable SearchDirectory(string searchText, string searchField)
        {
            return GetADsearch(searchText, searchField);
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText from firstname, surname and a contcatenated bothnames as datatable")]
        public DataTable SearchDirectoryAsDT(string searchText)
        {
            return GetADDataTableSearch(searchText);
        }

        [WebMethod(Description = "Returns a single AD user via their unique identifier (sAMAccountName) field")]
        public DataTable getUser(string ID)
        {
            return GetSingleUser(ID);
        }
        #endregion

        #region XML web methods

        [WebMethod(Description = "Returns all AD users as an XML doc")]
        public XmlDocument ListFullDirectoryXML()
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc = DTtoXML(GetADAllUsers());
            return XMLDoc;
        }

        [WebMethod(Description = "Returns all AD users with Fullname(cn) begining with startletter as XML doc")]
        public XmlDocument AtoZDirectoryXML(Char StartLetter)
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc = DTtoXML(GetADAtoZ(StartLetter));
            return XMLDoc;
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText in a single field searchfield as XML doc")]
        public XmlDocument SearchDirectoryXML(string searchText, string searchField)
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc = DTtoXML(GetADsearch(searchText, searchField));
            return XMLDoc;
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText from firstname, surname and a concatenated bothnames as XML doc")]
        public XmlDocument SearchDirectoryasDTXML(string searchText)
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc = DTtoXML(GetADDataTableSearch(searchText));
            return XMLDoc;
        }

        [WebMethod(Description = "Returns a single AD user via their unique identifier (sAMAccountName) field as XML doc")]
        public XmlDocument getUserXML(string ID)
        {
            XmlDocument XMLDoc = new XmlDocument();
            XMLDoc = DTtoXML(GetSingleUser(ID));
            return XMLDoc;
        }
        #endregion

        #region json web methods
        [WebMethod(Description = "Returns all AD users as JSON")]
        public string ListFullDirectoryJSON()
        {
            return DTtoJSON(GetADAllUsers());
        }

        [WebMethod(Description = "Returns all AD users with Fullname(cn) begining with startletter as JSON")]
        public string AtoZDirectoryJSON(Char StartLetter)
        {
            return DTtoJSON(GetADAtoZ(StartLetter));
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText in a single field searchfield as JSON")]
        public string SearchDirectoryJSON(string searchText, string searchField)
        {
            return DTtoJSON(GetADsearch(searchText, searchField));
        }

        [WebMethod(Description = "Returns all AD users with a wildcard search of searchText from firstname, surname and a concatenated bothnames as JSON")]
        public string SearchDirectoryasDTJSON(string searchText)
        {
            return DTtoJSON(GetADDataTableSearch(searchText));
        }


        [WebMethod(Description = "Returns a single AD user via their unique identifier (sAMAccountName) field as JSON")]
        public string getUserJSON(string ID)
        {
            return DTtoJSON(GetSingleUser(ID));
        }

        #endregion



        private DataTable GetADAllUsers()
        {

            try
            {
                //move thes insdie try catch
                string ldapServerName = System.Configuration.ConfigurationManager.AppSettings["LDAPServer"].ToString();
                string Searchstring = System.Configuration.ConfigurationManager.AppSettings["searchString"].ToString();
                string SearchFilter = System.Configuration.ConfigurationManager.AppSettings["searchFilter"].ToString();
                DirectoryEntry oRoot = new DirectoryEntry("LDAP://" + ldapServerName + "/" + Searchstring);
                DirectorySearcher search = new DirectorySearcher(oRoot);
                search.Sort.PropertyName = "cn";
                search.Filter = SearchFilter.Replace("&amp;", "&");
                DataTable RetTable = new DataTable("users");
                DataRow dr = null;

                NameValueCollection adsection = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
                foreach (string key in adsection)
                {
                    search.PropertiesToLoad.Add(adsection[key]);
                    RetTable.Columns.Add(key);
                }

                SearchResultCollection resultCol = search.FindAll();


                if (resultCol != null)
                {
                    foreach (SearchResult result in resultCol)
                    {
                        if (result.Properties[adsection[0]].Count > 0)
                        {
                            dr = RetTable.NewRow();
                            foreach (string key in adsection)
                            {
                                if (result.Properties[adsection[key]].Count > 0)
                                {
                                    dr[key] = result.Properties[adsection[key]][0];
                                }
                            }
                            RetTable.Rows.Add(dr);
                        }
                    }
                }

                RetTable.AcceptChanges();
                return RetTable;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetADAtoZ(char StartCharacter = ' ')
        {
            string ldapServerName = System.Configuration.ConfigurationManager.AppSettings["LDAPServer"].ToString();
            string Searchstring = System.Configuration.ConfigurationManager.AppSettings["searchString"].ToString();
            string SearchFilter = System.Configuration.ConfigurationManager.AppSettings["searchFilter"].ToString();
            try
            {
                if ((!String.IsNullOrEmpty(ldapServerName) && (!String.IsNullOrEmpty(Searchstring))))
                {
                    DirectoryEntry oRoot = new DirectoryEntry("LDAP://" + ldapServerName + "/" + Searchstring);
                    DirectorySearcher search = new DirectorySearcher(oRoot);
                    search.Sort.PropertyName = "cn";
                    search.Filter = SearchFilter.Replace("&amp;", "&");
                    if (StartCharacter != ' ')
                        search.Filter = "(&(" + search.Filter + ")(cn=" + StartCharacter + "*))";
                    DataTable RetTable = new DataTable("users");
                    DataRow dr = null;
                    NameValueCollection adsection = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
                    foreach (string key in adsection)
                    {
                        search.PropertiesToLoad.Add(adsection[key]);
                        RetTable.Columns.Add(key);
                    }

                    SearchResultCollection resultCol = search.FindAll();


                    if (resultCol != null)
                    {
                        foreach (SearchResult result in resultCol)
                        {
                            if (result.Properties[adsection[0]].Count > 0)
                            {
                                dr = RetTable.NewRow();
                                foreach (string key in adsection)
                                {
                                    if (result.Properties[adsection[key]].Count > 0)
                                    {
                                        dr[key] = result.Properties[adsection[key]][0];
                                    }
                                }
                                RetTable.Rows.Add(dr);
                            }
                        }
                    }

                    return RetTable;
                }
                else
                {
                    throw new KeyNotFoundException("Null or empty web.config values for 'LDAPServer' and/or 'searchString'");
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        private DataTable GetADsearch(string searchFor, string searchField)
        {
            string ldapServerName = System.Configuration.ConfigurationManager.AppSettings["LDAPServer"].ToString();
            string Searchstring = System.Configuration.ConfigurationManager.AppSettings["searchString"].ToString();
            string SearchFilter = System.Configuration.ConfigurationManager.AppSettings["searchFilter"].ToString();
            try
            {
                DirectoryEntry oRoot = new DirectoryEntry("LDAP://" + ldapServerName + "/" + Searchstring);
                DirectorySearcher search = new DirectorySearcher(oRoot);
                search.Sort.PropertyName = "cn";
                search.Filter = SearchFilter.Replace("&amp;", "&");
                if ((string.Compare(searchFor, "") > 0) && (string.Compare(searchField, "") > 0))
                    search.Filter = "(&(" + search.Filter + ")(" + searchField + "=*" + searchFor + "*))";
                DataTable RetTable = new DataTable("users");
                DataRow dr = null;
                NameValueCollection adsection = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
                foreach (string key in adsection)
                {
                    search.PropertiesToLoad.Add(adsection[key]);
                    RetTable.Columns.Add(key);
                }

                SearchResultCollection resultCol = search.FindAll();


                if (resultCol != null)
                {
                    foreach (SearchResult result in resultCol)
                    {
                        if (result.Properties[adsection[0]].Count > 0)
                        {
                            dr = RetTable.NewRow();
                            foreach (string key in adsection)
                            {
                                if (result.Properties[adsection[key]].Count > 0)
                                {
                                    dr[key] = result.Properties[adsection[key]][0];
                                }
                            }
                            RetTable.Rows.Add(dr);
                        }
                    }
                }
                return RetTable;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DataTable GetADDataTableSearch(string searchFor)
        {
            //this Tries to get around the full search option wherby an LDAP search of both name eg 'Niall Till' would retrn nothing
            //it uses the previously created DT talber, lodas it locally and then filter it on firstname, surname and the concatenated bothname field

            DataTable newTable;
            DataTable AllUsers = GetADAllUsers();
            AllUsers.Columns.Add("Bothnames", typeof(string));
            AllUsers.AcceptChanges();
            newTable = AllUsers.Clone();  //to get the same table structure but should be empty

            foreach (DataRow row in AllUsers.Rows)
            {
                row["Bothnames"] = (row["FirstName"].ToString() + " " + row["Surname"].ToString()).Trim();
            }
            AllUsers.AcceptChanges();

            DataView dv = new DataView(AllUsers);
            dv.RowFilter = "FirstName LIKE '%" + searchFor + "%' OR Surname LIKE '%" + searchFor + "%' OR Bothnames LIKE '%" + searchFor + "%' ";
            dv.Sort = "Surname ASC";
            foreach (DataRow dr in dv.ToTable().Rows)
            {
                newTable.ImportRow(dr);
            }
            newTable.AcceptChanges();
            return newTable;
        }


        private DataTable GetSingleUser(string id)
        {
            string ldapServerName = System.Configuration.ConfigurationManager.AppSettings["LDAPServer"].ToString();
            string Searchstring = System.Configuration.ConfigurationManager.AppSettings["searchString"].ToString();
            string SearchFilter = System.Configuration.ConfigurationManager.AppSettings["searchFilter"].ToString();
            try
            {
                DirectoryEntry oRoot = new DirectoryEntry("LDAP://" + ldapServerName + "/" + Searchstring);
                DirectorySearcher search = new DirectorySearcher(oRoot);
                search.Filter = SearchFilter.Replace("&amp;", "&");
                if ((string.Compare(id, "") > 0))
                    search.Filter = "(&(" + search.Filter + ")(sAMAccountName=" + id + "))";
                DataTable RetTable = new DataTable("users");
                DataRow dr = null;
                NameValueCollection adsection = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
                foreach (string key in adsection)
                {
                    search.PropertiesToLoad.Add(adsection[key]);
                    RetTable.Columns.Add(key);
                }

                SearchResultCollection resultCol = search.FindAll();


                if (resultCol != null)
                {
                    foreach (SearchResult result in resultCol)
                    {
                        if (result.Properties[adsection[0]].Count > 0)
                        {
                            dr = RetTable.NewRow();
                            foreach (string key in adsection)
                            {
                                if (result.Properties[adsection[key]].Count > 0)
                                {
                                    dr[key] = result.Properties[adsection[key]][0];
                                }
                            }
                            RetTable.Rows.Add(dr);
                        }
                    }
                }

                return RetTable;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        #region utilities

        public XmlDocument DTtoXML(DataTable dtIn)
        {
            XmlDocument XMLDoc = new XmlDocument();
            DataSet ds = new DataSet("directory");
            ds.Tables.Add(dtIn);
            ds.AcceptChanges();
            string strXML = ds.GetXml();
            XMLDoc.LoadXml(strXML);
            return XMLDoc;
        }

        private string DTtoJSON(DataTable dtIn)
        {
            JSONWriter json = new JSONWriter();
            NameValueCollection adsection = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
            try
            {
                json.BeginArray("users");
                foreach (DataRow row in dtIn.Rows)
                {
                    json.BeginObject();
                    foreach (string key in adsection)
                    {
                        json.AddProperty(adsection[key], row[key].ToString());
                    }
                    json.EndObject();
                }
                json.EndArray();
                json.AddBooleanProperty("Success", true);
                json.AddProperty("ErrorMessage", "");

                Context.Response.ContentType = "application/json";
                Context.Response.Write(json.GetJSON());
                Context.Response.End();

                return string.Empty;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}




