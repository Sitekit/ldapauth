﻿using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Diagnostics;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;
using System.Web;
using System.Xml;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;

namespace LdapAuthCS
{
    /// <summary>
    // Version 3.1; 23/9/9, CE now has Domain prefix added. 
    // Each LDAP server will need to have a domain prefix specified in web.config. This can be empty.
    // e.g. for 2 ldap servers this could be:
    //  add key="DomainPrefix" value="," />
    // Version 3.1a Minor error fix onthe DomainPrefix
    //Version 3.1b Introduced the variable ReturnError in the ReturnGroups function 
    // to allow not showing errors is one only want's to return the groups.
    // v4 support for groups added, 
    // v6 changed to code behind for efficiency, incorpoates group stuff from earlier versions. CS version of vb file
    /// </summary>
    [WebService(Namespace = "http://www.sitekit.net/LDAPUtility/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class LDAPAuthv6 : System.Web.Services.WebService
    {

        ArrayList groups = new ArrayList();
        DirectoryEntry entry;
        DirectoryEntry rootentry;


        [WebMethod(Description = "simple test taking in username password and ldap server eg  LDAP://gnsbs01")]
        public String testAuthSimple(string username, string password, string ldapserver)
        {
            // create representation of entry in Active Directory
            entry = new DirectoryEntry(ldapserver, username, password);
           // Bind entry to the native AdsObject to force authentication.
            object obj = entry.NativeObject;
            return entry.ToString();
        }

        [WebMethod(Description = "simple test taking in username password and ldap server eg  LDAP://gnsbs01 with exception catch")]
        public String testAuthSimpleWithEx(string username, string password, string ldapserver)
        {
            try
            {
                // create representation of entry in Active Directory
                entry = new DirectoryEntry(ldapserver, username, password);
                // Bind entry to the native AdsObject to force authentication.
                object obj = entry.NativeObject;
                return entry.ToString();
            }
            catch (Exception ex)
            {
                return "Error: " + ex.ToString();
            }
        }

        [WebMethod(Description = "simple test taking in username password and ldapserver eg  gnsbs01 (no protocol)  using PrincipalContext")]
        public String testAuthSimple2(string username, string password, string ldapserver)
        {

            var principalContext = new PrincipalContext(ContextType.Domain, ldapserver, username, password);
            var validated = principalContext.ValidateCredentials(username, password);
            var userPrincipal = UserPrincipal.FindByIdentity(principalContext, username);
            return userPrincipal.ToString();
        }

        [WebMethod(Description = "simple test taking in username password and ldapserver eg  gnsbs01 (no protocol)  using PrincipalContext")]
        public String testAuthSimple2Ex(string username, string password, string ldapserver)
        {
            try
            {
            var principalContext = new PrincipalContext(ContextType.Domain, ldapserver, username, password);
            var validated = principalContext.ValidateCredentials(username, password);
            var userPrincipal = UserPrincipal.FindByIdentity(principalContext, username);
            return userPrincipal.ToString();                       
            }
            catch (Exception ex)
            {
                return "Error: " + ex.ToString();
            }
        }

        [WebMethod(Description = "Authenticates specified user details against specified LDAP server. This function with authenticated users in nested groups, too. Can not check against groups like Domain Users for this case leave group empty and it only authenticates.")]
        public XmlDocument IsAuthenticated(string username, string password, string @group)
        {
            string[] LDAPServers = null;
            string[] DomainPrefix = null;
            XmlDocument Result = new XmlDocument();
            LDAPServers = System.Configuration.ConfigurationManager.AppSettings["LDAPServers"].ToString().Split(',');
            DomainPrefix = System.Configuration.ConfigurationManager.AppSettings["DomainPrefix"].ToString().Split(',');
            for (Int16 i = 0; i <= LDAPServers.Length - 1; i++)
            {
                string UsernameToCheck = null;
                if (!UsernameHasDomain(username))
                {
                    UsernameToCheck = DomainPrefix[i].Trim() + "\\" + username.Trim();
                }
                else
                {
                    UsernameToCheck = username;
                }
                Result = IsAuthenticatedAgainstLDAP(UsernameToCheck, password, @group, LDAPServers[i]);
                if (Result.InnerText == true.ToString())
                    break; // TODO: might not be correct. Was : Exit For
            }
            return Result;
        }

        [WebMethod(Description = "Returns all groups of an specified user. This function with authenticated users in nested groups, too. If ReturnError is set to true a error is returned from each server the user has no rights on, but it's good for debugging.")]
        public string ReturnGroups(string username, string password, bool ReturnError)
        {
            string[] LDAPServers = null;
            string[] DomainPrefix = null;
            string Results = "";
            LDAPServers = System.Configuration.ConfigurationManager.AppSettings["LDAPServers"].ToString().Split(',');
            DomainPrefix = System.Configuration.ConfigurationManager.AppSettings["DomainPrefix"].ToString().Split(',');
            for (Int16 i = 0; i <= LDAPServers.Length - 1; i++)
            {
                string UsernameToCheck = null;
                if (!UsernameHasDomain(username))
                {
                    UsernameToCheck = DomainPrefix[i].Trim() + "\\" + username.Trim();
                }
                else
                {
                    UsernameToCheck = username;
                }
                Results += ReturnGroupsAgainstLDAP(UsernameToCheck, password, LDAPServers[i], ReturnError);
            }
            return Results;
        }

        /// <summary>
        /// Authenticates against a given LDAP server. Get's all the groups associated (including nested supergroups. Can not check against groups like "Domain Users" for this case leave group empty and it only authenticates.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="group"></param>
        /// <param name="LDAPServer"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private XmlDocument IsAuthenticatedAgainstLDAP(string username, string password, string @group, string LDAPServer)
        {

            // get path to LDAP server from web.config
            string path = LDAPServer;
            string usernameWithoutDomain = null;

            try
            {
                usernameWithoutDomain = getUsernameWithoutDomain(username);

                // create representation of entry in Active Directory
                entry = new DirectoryEntry(path, usernameWithoutDomain, password);

                // Bind entry to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + usernameWithoutDomain + ")";
                search.PropertiesToLoad.Add("cn");
                //search.PropertiesToLoad.Add("telephoneNumber");
                SearchResult result = search.FindOne();



                if ((result == null))
                {
                    return ResultXML(false, "No user found or no connection to the LDAP server");
                }
                else
                {
                    if (!string.IsNullOrEmpty(@group))
                    {
                        // Dim groups As ArrayList
                        GetGroups(result, username, password, LDAPServer);
                        if (groups.Contains(@group.ToLower()))
                        {
                            return ResultXML(true, "Success, user found in " + @group.ToLower());
                        }
                        else
                        {
                            return ResultXML(false, "User found, but this user is not part of the queried group");
                        }
                    }
                    else
                    {
                        return ResultXML(true, "Success, user found in Domain users");
                    }

                }

            }
            catch (Exception ex)
            {
                return ResultXML(false, "Error: " + ex.ToString());
                //Throw ex
            }

        }

#region "Integrated Active Directory"

       
        [WebMethod(Description = "Authenticates specified user details against specified LDAP server. This function with authenticated users in nested groups, too. Can not check against groups like Domain Users for this case leave group empty and it only authenticates.")]
        public XmlDocument GetActiveDirectoryEntry(string username, string password, string @group)
        {
            string[] LDAPServers = null;
            string[] DomainPrefix = null;
            XmlDocument Result = new XmlDocument();
            LDAPServers = System.Configuration.ConfigurationManager.AppSettings["LDAPServers"].ToString().Split(',');
            DomainPrefix = System.Configuration.ConfigurationManager.AppSettings["DomainPrefix"].ToString().Split(',');
            for (Int16 i = 0; i <= LDAPServers.Length - 1; i++)
            {
                string UsernameToCheck = null;
                if (!UsernameHasDomain(username))
                {
                    UsernameToCheck = DomainPrefix[i].Trim() + "\\" + username.Trim();
                }
                else
                {
                    UsernameToCheck = username;
                }
                Result = GetActiveDirectoryXML(UsernameToCheck, password, @group, LDAPServers[i]);
                if (Result.InnerText == true.ToString())
                    break; // TODO: might not be correct. Was : Exit For
            }
            return Result;
        }

        private XmlDocument GetActiveDirectoryXML(string username, string password, string @group, string LDAPServer)
        {

            // get path to LDAP server from web.config
            string path = LDAPServer;
            string usernameWithoutDomain = null;

            try
            {
                usernameWithoutDomain = getUsernameWithoutDomain(username);

                // create representation of entry in Active Directory
                entry = new DirectoryEntry(path, usernameWithoutDomain, password);

                // Bind entry to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + usernameWithoutDomain + ")";

                //for (int i = 0; i <= SKNameToADNameMappings.Length / 2 - 1; i++)
                //{
                //    search.PropertiesToLoad.Add(SKNameToADNameMappings[i, 1]);
                //}

                //revised to use the name value pair collection
                NameValueCollection NameToADNameCol = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
                string s = null;
                foreach (string s_loopVariable in NameToADNameCol.AllKeys)
                {
                    s = s_loopVariable;
                    search.PropertiesToLoad.Add(NameToADNameCol[s]);
                }
                SearchResult result = search.FindOne();

                if ((result == null))
                {
                    return ResultXML(false, "No user found or no connection to the LDAP server");
                }
                else
                {
                    if (!string.IsNullOrEmpty(@group))
                    {
                        // Dim groups As ArrayList
                        GetGroups(result, username, password, LDAPServer);
                        if (groups.Contains(@group.ToLower()))
                        {
                            return ADUserXML(true, ref result);

                        }
                        else
                        {
                            return ResultXML(false, "User found, but this user is not part of the queried group");
                        }
                    }
                    else
                    {
                        return ADUserXML(true, ref result);
                    }

                }

            }
            catch (Exception ex)
            {
                return ResultXML(false, "Error: " + ex.ToString());
                //Throw ex
            }

        }

        // returns 'true' or 'false' in an xml document (format expected by Sitekit 3rd Party Authentication)
        private XmlDocument ADUserXML(bool authentic, ref SearchResult result)
        {

            //<user>" & _
            //                                    "   <authenticated>true</authenticated>" & _
            //                                    "   <userparam name=""Fullname"">Andy Stables</userparam>" & _
            //                                    "   <userparam name=""Eye Colour"">Dingy</userparam>" & _
            //                                    "</user>
            XmlDocument xmlDoc = new XmlDocument();
            XmlElement authenticatedElement = default(XmlElement);
            XmlElement rootElement = default(XmlElement);

            rootElement = xmlDoc.CreateElement("user");

            authenticatedElement = xmlDoc.CreateElement("authenticated");
            authenticatedElement.InnerText = authentic.ToString();
            rootElement.AppendChild(authenticatedElement);

            //for (int i = 0; i <= SKNameToADNameMappings.Length / 2 - 1; i++)
            //{
            //    XmlElement userParamElement = xmlDoc.CreateElement("userparam");
            //    userParamElement.SetAttribute("name", SKNameToADNameMappings[i, 0]);
            //    if (result.Properties[SKNameToADNameMappings[i, 1]].Count > 0)
            //    {
            //        userParamElement.InnerText = result.Properties[SKNameToADNameMappings[i, 1]][0].ToString();
            //        rootElement.AppendChild(userParamElement);
            //    }
            //}

            //revised to use the name value pair collection
            NameValueCollection NameToADNameCol = (NameValueCollection)System.Configuration.ConfigurationManager.GetSection("ADMap");
            string s = null;
            foreach (string s_loopVariable in NameToADNameCol.AllKeys)
            {
                 s = s_loopVariable;
                 XmlElement userParamElement = xmlDoc.CreateElement("userparam");
                 userParamElement.SetAttribute("name", s);
                 if (result.Properties[NameToADNameCol[s]].Count > 0)
                 {
                    userParamElement.InnerText = result.Properties[NameToADNameCol[s]][0].ToString();
                    rootElement.AppendChild(userParamElement);
                 }
            }

            //this section added from v5 version to v4
            XmlElement groupsNode = xmlDoc.CreateElement("groups");

            foreach (string groupName in groups)
            {
                XmlElement @group = xmlDoc.CreateElement("group");
                @group.InnerText = groupName;
                groupsNode.AppendChild(@group);
            }
            rootElement.AppendChild(groupsNode);
            //end section

            xmlDoc.AppendChild(rootElement);

            return xmlDoc;

        }

        #endregion

        // retrieves all groups specified user is a member of

        private void GetGroups(SearchResult userResult, string username, string password, string LDAPServer)
        {
            string cn = null;
            // "common name" - LDAP attribute made from givenName joined to SN (surname)
            string dn = null;
            // "distinguished name" - LDAP attribute that uniquely defines an object in Active Directory
            string groupName = null;
            string usernameWithoutDomain = null;
            // name of group in Active Directory
            // Dim groups As New ArrayList

            DirectorySearcher search = default(DirectorySearcher);
            SearchResult result = default(SearchResult);

            int equalsIndex = 0;
            int commaIndex = 0;
            int propertyCounter = 0;

            try
            {
                usernameWithoutDomain = getUsernameWithoutDomain(username);
                // create representation of entry in Active Directory
                entry = new DirectoryEntry(userResult.Path, usernameWithoutDomain, password);

                // Bind entry to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                // do full search on specified user to retrieve those groups user is a member of
                search = new DirectorySearcher(entry);
                cn = Convert.ToString(userResult.Properties["cn"][0]);
                search.Filter = "(cn=" + cn + ")";
                search.PropertiesToLoad.Add("memberOf");
                result = search.FindOne();

                // extract the group name from each dn 
                for (propertyCounter = 0; propertyCounter <= result.Properties["memberOf"].Count - 1; propertyCounter++)
                {
                    dn = Convert.ToString(result.Properties["memberOf"][propertyCounter]);

                    // extract 1st attribute from dn
                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);
                    if ((equalsIndex != -1))
                    {
                        groupName = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1);
                        //only add if it's not in the array list already
                        if (!groups.Contains(groupName.ToLower()))
                        {
                            groups.Add(groupName.ToLower());
                            //seach for this group now
                            //get a root entry to search ALL LDAP for this group not jsut under the user
                            string path = LDAPServer;

                            // create representation of entry in Active Directory
                            rootentry = new DirectoryEntry(path, username, password);

                            // Bind entry to the native AdsObject to force authentication.
                            obj = rootentry.NativeObject;

                            DirectorySearcher search1 = new DirectorySearcher(rootentry);
                            search1.Filter = "(SAMAccountName=" + groupName.ToLower() + ")";
                            search1.PropertiesToLoad.Add("cn");
                            SearchResult result1 = search1.FindOne();
                            if ((result1 != null))
                                GetGroups(result1, username, password, LDAPServer);
                            //if it's in the arraylist already finish loop
                        }
                        else
                        {
                            // Exit For
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error obtaining group names. " + ex.Message);
            }



        }


        // retrieves all groups specified user is a member of
        private ArrayList GetGroupsV2(SearchResult userResult)
        {

            string cn = null;
            // "common name" - LDAP attribute made from givenName joined to SN (surname)
            string dn = null;
            // "distinguished name" - LDAP attribute that uniquely defines an object in Active Directory
            string groupName = null;
            // name of group in Active Directory
            ArrayList groups = new ArrayList();
            DirectorySearcher search = default(DirectorySearcher);
            SearchResultCollection resultCollection = default(SearchResultCollection);
            int equalsIndex = 0;
            int commaIndex = 0;
            int propertyCounter = 0;

            // Do full search on specified user to retrieve those groups user is a member of
            search = new DirectorySearcher(userResult.Path);
            cn = Convert.ToString(userResult.Properties["cn"][0]);
            search.Filter = "(cn=" + cn + ")";
            search.PropertiesToLoad.Add("memberOf");

            resultCollection = search.FindAll();

            foreach (SearchResult result in resultCollection)
            {
                // Extract the group name from each dn 

                for (propertyCounter = 0; propertyCounter <= result.Properties["memberOf"].Count - 1; propertyCounter++)
                {
                    dn = Convert.ToString(result.Properties["memberOf"][propertyCounter]);

                    // Extract 1st attribute from dn
                    equalsIndex = dn.IndexOf("=", 1);
                    commaIndex = dn.IndexOf(",", 1);

                    if ((equalsIndex != -1))
                    {
                        groupName = dn.Substring((equalsIndex + 1), (commaIndex - equalsIndex) - 1);
                        groups.Add(groupName.ToLower());
                    }
                }
            }

            return groups;
        }



        // returns 'true' or 'false' in an xml document (format expected by Sitekit 3rd Party Authentication)
        private XmlDocument ResultXML(bool authenticated, string reason = "")
        {

            XmlDocument xmlDoc = new XmlDocument();
            XmlElement elAuth = default(XmlElement);

            elAuth = xmlDoc.CreateElement("authenticated");
            elAuth.InnerText = authenticated.ToString();
            if (!string.IsNullOrEmpty(reason))
            {
                elAuth.SetAttribute("Reason", reason.ToString());
            }
            xmlDoc.AppendChild(elAuth);


            return xmlDoc;

        }





        private string ReturnGroupsAgainstLDAP(string username, string password, string LDAPServer, bool ReturnError)
        {

            // get path to LDAP server from web.config
            string path = LDAPServer;
            string usernameWithoutDomain = null;

            try
            {
                usernameWithoutDomain = getUsernameWithoutDomain(username);

                // create representation of entry in Active Directory
                entry = new DirectoryEntry(path, username, password);

                // Bind entry to the native AdsObject to force authentication.
                object obj = entry.NativeObject;

                DirectorySearcher search = new DirectorySearcher(entry);
                search.Filter = "(SAMAccountName=" + usernameWithoutDomain + ")";
                search.PropertiesToLoad.Add("cn");
                SearchResult result = search.FindOne();


                if ((result == null))
                {
                    return "No user found or no connection to the LDAP server";
                }
                else
                {
                    // Dim groups As ArrayList
                    GetGroups(result, username, password, LDAPServer);
                    string res = "";
                    foreach (string s in groups)
                    {
                        res = res + ":" + s;
                    }

                    res += "EMAIL = " + entry.Properties["mail"].Value;
                    res += "EMAIL = " + entry.Properties["cnn"].Value;

                    return res;
                }

            }
            catch (Exception ex)
            {
                if (ReturnError)
                    return "Error: " + ex.ToString();
                else
                    return "";
                //Throw ex
            }

        }


        private string getUsernameWithoutDomain(string username)
        {
            if (username.IndexOf("\\") > -1)
            {
                return username.Substring(username.IndexOf("\\") + 1);
            }
            else
            {
                return username;
            }
        }

        private bool UsernameHasDomain(string username)
        {
            if (username.IndexOf("\\") > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
